# speedwm-extras

Extra scripts for speedwm, most of them use [spmenu](https://codeberg.org/speedie/spmenu) by default. You can change it to dmenu by exporting $RUNLAUNCHER.

NOTE: Most of the scripts **require** [libspeedwm](https://codeberg.org/speedie/libspeedwm).

## Usage with speedwm-swal

speedwm-swal requires extra steps. To use it with speedwm: Add `$HOME/.config/speedwm/swal/swal_wm; libspeedwm --perform core_wm_reload` to ~/.config/speedwm/autostart.sh and make it executable using `chmod +x ~/.config/speedwm/swal/autostart.sh`.
